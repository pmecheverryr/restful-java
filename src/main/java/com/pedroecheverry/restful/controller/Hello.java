package com.pedroecheverry.restful.controller;

import com.pedroecheverry.restful.entity.Greeting;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class Hello {

    @GetMapping("world")
    public ResponseEntity<Greeting> greeting() {
        Greeting greeting = new Greeting();
        greeting.setId(this.randomId());
        greeting.setMessage("Hello World");
        return ResponseEntity.ok(greeting);
    }

    private Integer randomId(){
        return (int) (Math.random() * 100 + 1);
    }

}
